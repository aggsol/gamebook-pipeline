**WORK IN PROGRESS**

# gamebook-pipeline v0.2.1

Write, create and publish your own gamebook

All my tools, scripts, macros, templates and helping instructions to write a gamebook and create PDF and ePub versions for distribution.
The gamebook is written in [Pandoc's Markdown flavour](https://pandoc.org/MANUAL.html#pandocs-markdown).

The tools has only been tested on Ubuntu (sorry!) but should work on any GNU/Linux providing recent versions of Pandoc and Texlive.

## Alternatives

Have a look at

* [gamebookformat](https://github.com/lifelike/gamebookformat)
* [The GameBook Authoring Tool](https://www.crumblyheadgames.co.uk/the-gamebook-authoring-tool/)
* [Twine](https://twinery.org/)

## Required Tools & Packages

While writing the gamebook the [gamebook-tool](https://gitlab.com/aggsol/gamebook-tool) provides error checking and visualisation of written sections.

The pipleline to create PDF and ePub gamebooks requires

* Pandoc
* Texlive (xelatex)
* GPP (Generic Preprocessor)
* GNU Make
* pdfbook
* graphviz

```sh
$ apt install \
	pandoc \
	gpp \
	texlive \
	texlive-xetex \
	texlive-latex-extra \
	texlive-lang-german \
	texlive-fonts-extra \
	texlive-extra-utils \
	graphviz
```

For building the `gamebook-tool` you need

* C++ compiler
* CMake

Make sure that you clone the repo recursively or use `git submodule update --init --recursive` afterwards.

```sh
$ apt install cmake g++ clang++
$ cd gamebook-tool
$ ./build-release.sh
```

## Example

In the folder `example`

## Licenses

* **gamebook-pipeline is GPLv3 licensed**

### Dependencies

* [Pandoc Lua Filters are MIT licensed](https://github.com/pandoc/lua-filters)
