FROM gitpod/workspace-full

USER root

RUN sudo apt-get update

RUN sudo apt-get install -y \
    pandoc \
    graphviz \
    lcov \
    texlive \
    texlive-xetex \
    texlive-latex-extra \
    texlive-lang-german \
    texlive-fonts-extra \
    texlive-extra-utils \
    gpp
