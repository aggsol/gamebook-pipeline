#!/usr/bin/env bash
#
#
set -euo pipefail
IFS="$(printf '\n\t')"

readonly version="1.0.0"
readonly scriptname="${0##*/}"
function printUsage()
{
    echo "${scriptname} ${version}"
    echo "Usage: ${scriptname} -i myfile.adoc -o myfile.md"
    echo "Convert asciidoc to markdown"
    echo ""
    echo "-i, --input <file>    Input asciidoc file. Mandatory"
    echo "-o, --output <file>   Output file."
    echo "-h, --help            Show this help"
    exit 0
}

function error()
{
    echo "[Error]: $*" >&2
}

# set default values
input=""
output=""
help=false
declare -a positional=()

while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -i|--input)
    readonly input="$2"
    shift # past argument
    shift # past value
    ;;
    -o|--output)
    readonly output="$2"
    shift # past argument
    shift # past value
    ;;
    -h|--help)
    readonly help=true
    shift # past argument
    ;;
    *)    # unknown option
    positional+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${#positional[@]}" # restore positional parameters

if [[ "$help" == true ]]; then
    printUsage
fi

if [[ -z "$input" ]]; then
    printUsage
fi

if [[ ! -f "$input" ]]; then
    error "$input does not exist. Abort."
    exit 101
fi

if [[ ! $input =~ \.adoc$ ]]; then
    error "$input is not Asciidoc. Abort."
fi

if [[ -z "$output" ]]; then
    output="${input%.adoc}.md"
fi

#echo "input = ${input}"
#echo "output = ${output}"

asciidoctor -b docbook -a leveloffset=+1 -o - "$input" | pandoc  --atx-headers --wrap=preserve -t markdown_strict -f docbook - > "$output"
