# Manual

The gamebook-pipeline allows to write a gamebook in Pandoc's Markdown and publish it as a print-ready PDF and e-book in ePub format.
This includes embedding illustrations, flow of sections and writing help.